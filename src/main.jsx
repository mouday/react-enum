import React from 'react';
import ReactDOM from 'react-dom/client';
import { Provider } from 'react-redux';
import { HashRouter } from 'react-router-dom';
import { ConfigProvider } from 'antd';
import zhCN from 'antd/locale/zh_CN';
import App from './App.jsx';
import './main.css';
import { initGlobalData } from './store/global-data.js';
import store from './store/index.js';

const root = ReactDOM.createRoot(document.getElementById('root'));

(async () => {
  // 初始化全局数据
  await initGlobalData();
  console.log('initGlobalData');

  root.render(
    <React.StrictMode>
      <ConfigProvider locale={zhCN}>
        <Provider store={store}>
          <HashRouter>
            <App />
          </HashRouter>
        </Provider>
      </ConfigProvider>
    </React.StrictMode>,
  );
})();

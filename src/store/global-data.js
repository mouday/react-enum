import http from "../api/index.js";

/**
 * 全局静态数据
 */
export const globalData = {
  SexEnum: [],
  ColorEnum: [],
};

// 初始化
export async function initGlobalData() {
  const res = await http.get("/getEnumConfig");

  for (const key in globalData) {
    globalData[key] = res[key];
  }
}

// === getter ===
export function getSexEnumFilterOptions() {
  console.log(globalData);

  return [{ value: "", label: "全部" }, ...globalData.SexEnum];
}

export function getSexEnumOptions() {
  return globalData.SexEnum;
}

export function getColorEnumOptions() {
  return globalData.ColorEnum;
}

import { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { Navigate, NavLink, useRoutes } from 'react-router-dom';
import ButtonGroup from './components/ButtonGroup/ButtonGroup.jsx';
import DataTable from './components/DataTable/DataTable.jsx';
import NavMenu from './components/NavMenu/NavMenu.jsx';
import http from './api/index.js';
import './App.css';
import { getColorEnumColor } from './enums/index.js';
import CreateRouter from './router/inde.jsx';
import { createUserStore } from './store/index.js';

function App(props) {
  const { syncUserInfoAsync } = props;

  useEffect(() => {
    syncUserInfoAsync();
  }, []);

  return (
    <div className="app">
      {/* <NavMenu /> */}

      {/* 注册路由 */}
      <div className="mt-md">
        <CreateRouter />
      </div>
    </div>
  );
}

export default createUserStore(App);

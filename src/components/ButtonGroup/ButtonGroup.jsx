import React from "react";
import { Radio } from "antd";
import { connect } from "react-redux";

import { getSexEnumFilterOptions } from "../../store/global-data.js";

export default function ButtonGroup(props) {
  const SexEnumOptions = getSexEnumFilterOptions();

  const { value, onChange } = props;

  return (
    <Radio.Group
      options={SexEnumOptions}
      onChange={onChange}
      value={value}
      optionType="button"
      buttonStyle="solid"
    />
  );
}

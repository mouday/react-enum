import React, { useState } from "react";
import { Menu } from "antd";
import { useNavigate } from "react-router-dom";

const items = [
  {
    label: "列表",
    key: "/list",
  },
  {
    label: "编辑",
    key: "/edit",
  },
];

export default function NavMenu() {
  const [current, setCurrent] = useState("mail");
  const navigate = useNavigate();
  const onClick = (e) => {
    console.log(e);
    setCurrent(e.key);

    navigate(e.key);
  };

  return (
    <Menu
      onClick={onClick}
      selectedKeys={[current]}
      mode="horizontal"
      items={items}
    />
  );
}

import React, { useEffect, useState } from "react";
import { Table, Tag, message, Popconfirm } from "antd";
import { ColorEnum } from "../../enums/index.js";
import { EditOutlined, DeleteOutlined } from "@ant-design/icons";
import { useNavigate } from "react-router-dom";
import http from "../../api/index.js";

const columns = [
  {
    title: "姓名",
    dataIndex: "name",
    align: "center",
  },
  {
    title: "性别",
    dataIndex: "sexLabel",
    align: "center",
  },
  {
    title: "颜色",
    dataIndex: "colorLabel",
    align: "center",
    render: (text, record, index) => {
      return <Tag color={record.colorName}>{text}</Tag>;
    },
  },
  {
    title: "编辑",
    dataIndex: "edit",
    align: "center",
    width: "80px",
    render: (text, record, index) => {
      return <Edit id={record.id}></Edit>;
    },
  },
  {
    title: "移除",
    dataIndex: "delete",
    align: "center",
    width: "80px",
    render: (text, record, index) => {
      return <Delete id={record.id} onRefresh={record.onRefresh}></Delete>;
    },
  },
];

function Delete(props) {
  const { id, onRefresh } = props;

  const navigate = useNavigate();

  const onClick = async () => {
    const res = await http.delete(`/deleteUser?id=${id}`);

    message.success("操作成功");

    onRefresh();
  };

  return (
    <div className="cursor-pointer" style={{ color: "#ff4d4f" }}>
      <Popconfirm title="确认删除？" onConfirm={onClick}>
        <DeleteOutlined />
      </Popconfirm>
    </div>
  );
}

function Edit(props) {
  const { id } = props;

  const navigate = useNavigate();

  const onClick = () => {
    navigate(`/edit/${id}`);
  };

  return (
    <div
      onClick={onClick}
      className="cursor-pointer"
      style={{ color: "#1677ff", cursor: 'pointer' }}
    >
      <EditOutlined />
    </div>
  );
}

export default function DataTable(props) {
  // 表格数据
  return (
    <Table
      bordered
      columns={columns}
      {...props}
    />
  );
}

/**
 * 颜色枚举，前端代码需要逻辑判断
 */
export const ColorEnum = {
  // 红色
  RED: 'RED',
  // 绿色
  GREEN: 'GREEN',
  // 蓝色
  BLUE: 'BLUE',
};

export const ColorEnumOptions = [
  {
    // 红色
    value: ColorEnum.RED,
    color: 'error',
  },
  {
    // 绿色
    value: ColorEnum.GREEN,
    color: 'success',
  },
  {
    // 蓝色
    value: ColorEnum.BLUE,
    color: 'processing',
  },
];

export function getColorEnumColor(value) {
  return (
    ColorEnumOptions.find((item) => item.value === value)?.color || 'default'
  );
}

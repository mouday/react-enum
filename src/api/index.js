import axios from "axios";

const instance = axios.create({
  baseURL: "http://localhost:8080",
});

// 请求拦截器
instance.interceptors.request.use(
  (config) => {
    return config;
  },
  () => {}
);

// 添加响应拦截器
instance.interceptors.response.use(
  (res) => {
    return res.data;
  },
  () => {}
);

export default instance;

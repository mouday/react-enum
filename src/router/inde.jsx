import List from "../pages/List/List.jsx";
import Edit from "../pages/Edit/Edit.jsx";
import { NavLink, Navigate, useRoutes } from "react-router-dom";
import React from "react";

export default function CreateRouter() {
  const routes = useRoutes([
    {
      path: "/list",
      element: <List />,
    },
    {
      path: "/add",
      element: <Edit />,
    },
    {
      path: "/edit/:id",
      element: <Edit />,
    },
    {
      path: "/",
      element: <Navigate to="/list" />,
    },
  ]);

  // 注册路由
  return <>{routes}</>;
}

/** @type {import('tailwindcss').Config} */
export default {
  content: ['./src/**/*.{js,jsx,ts,tsx}'],
  theme: {
    extend: {},

    spacing: {
      sm: '10px',
      md: '20px',
      lg: '40px',
      xl: '60px',
    }
  },
  plugins: [],
  // 禁用 `清除浏览器默认样式` ref: https://blog.csdn.net/qq_37214567/article/details/125154970
  corePlugins: {
    preflight: false,
  },
};

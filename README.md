# React + Vite

使用到的技术栈：

- 语言：JavaScript
- 开发：Vite
- UI：React
- 状态：react-redux
- 路由：react-router
- 网络：axios
- 样式：Ant Design + Tailwind CSS
- 图标：@ant-design/icons
- 代码：ESLint + Prettier


参考：https://zhuanlan.zhihu.com/p/552344435
